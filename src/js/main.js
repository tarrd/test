'use strict'
$('.select select').select2({
  minimumResultsForSearch: -1
})

//slider start 17:20
let sliderShadow = $('.slider-shadow')
let sliderBtn = $('.slider-btn')
let draggable = false

function sliderMove(e) {
  let mousePos = e.pageX
  let parentPos = $('.slider').offset().left
  let currPos = parentPos - mousePos + 12
  let shadowWidth = parentPos - mousePos

  if (mousePos - parentPos < 10) {
    sliderBtn.css('left', '0')
    sliderShadow.css('width', '0')
  } else {
    sliderBtn.css('left', Math.abs(currPos) + 'px')
    sliderShadow.css('width', Math.abs(shadowWidth) + 'px')
    if (mousePos - parentPos > 768) {
      sliderBtn.css('left', 768 - 12 + 'px')
      sliderShadow.css('width', '768px')

    }
  }
}

$(document).on({
  'mousedown': function (e) {
    if (e.target.className == 'slider-btn') {
      draggable = true
    }

  },
  'mousemove': function (e) {
    if (draggable) {
      sliderMove(e)
    }
  },
  'mouseup': function () {
    draggable = false
  }
});

//click on slider
$('.slider-mask').on('click', function(e){
  sliderMove(e)
});
//end 18:10

//===============
//pause 1 hour
//start 19:10
//===============

//anchour
$('a[href^="#"]').click(function () {
  let target = $(this).attr('href');
  $('html, body').animate({ scrollTop: $(target).offset().top - 70 }, 800);
  return false;
});

//input focus\blur
$('.info-input').on({
  'focus': function () {
    $(this).parent().addClass('info-input--active')
  },
  'blur': function () {
    if($(this).val() !== '') {
      return false
    }
    $(this).parent().removeClass('info-input--active')
  }
});

//input check if value exist
$('.info-input').each(function(){
  if($(this).val() !== '') {
    $(this).parent().addClass('info-input--active')
  }
})

//header fixed on scroll
$(document).on('scroll', function(){
  if($(document).scrollTop() > 20){
    $('.header').addClass('header-active')
  } else {
    $('.header').removeClass('header-active')
  }
})

//mobile menu
var menu = $('.menu');
$('.menu-mobile').on('click', function(){
    $('._1' ,this).toggleClass('_1--active');
    $('._2' ,this).toggleClass('_2--active');
    $('._3' ,this).toggleClass('_3--active');
    menu.toggleClass('menu-show');
});

//end 20:05